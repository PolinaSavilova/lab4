define bigbob position
(
    # actual size
    size [1.25 1 1]
    
    # centre of rotation offset
    origin [0.125 0 0 0]
         
    # the shape of Bigbob
    block
    (
        points 6
        point[5] [0 0]
        point[4] [0 1]
        point[3] [0.75 1]
        point[2] [1 0.75]
        point[1] [1 0.25]
        point[0] [0.75 0]
        z [0 1]
    )

    # positonal things
    drive "diff"
    
    # sensors attached to bigbob
    bigbobs_sonars()
)

define bigbobs_sonar sensor
(
    # define the size of each transducer [xsize ysize zsize] in meters
    size [0.01 0.05 0.01 ] 
    # define the range bounds [min max]
    range [1 5]
    # define the angular field of view in degrees
    fov 140
    # define the color that ranges are drawn in the gui
    color_rgba [ 1 0 1 0 ] 
    samples 361
)

define bigbobs_sonars ranger
( 
  # one line for each sonar [xpos ypos zpos heading]
  bigbobs_sonar( pose [ 0.75 0 0 0]) 
)
